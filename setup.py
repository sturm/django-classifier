# Want to install straight from a git repo, rather than building a package, but
# it complains about a missing setup.py. Not sure if this is solveable or not.
#
# https://packaging.python.org/tutorials/packaging-projects/
# https://setuptools.readthedocs.io/en/latest/userguide/declarative_config.html

import setuptools

setuptools.setup()
