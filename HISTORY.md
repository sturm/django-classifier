# Release History

## 0.1.0 (2022-08-26)

 - Add some manual labelling to improve performance on non-English text and HTML
 - Add admin filter for auto and manual spam status
 - Update URLConfs for Django 4


## 0.0.7 (2021-10-01)

 - Add admin actions to bulk mark spam/not-spam
 - Add tox


## 0.0.6 (2021-03-15)

 - Respond with a 404 if a classifier submission doesn't exist
