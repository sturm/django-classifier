FROM alpine:3.13

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apk add --no-cache python3 py3-pip
RUN python3 -m pip install Django pytz pytest pytest-django
COPY . /srv/classifier
WORKDIR /srv/classifier
RUN PYTHONPATH=. pytest
